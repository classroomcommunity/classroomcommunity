Setting up Docker, docker-compose and docker-machine

...

Start the docker-machine:

	docker-machine start
	eval $(docker-machine env default)

Some versions of docker don't mount the home directory, which we need for development.
TODO: add more information about this.

	docker-machine ssh default 'sudo mkdir --parents /home/jason'
	docker-machine ssh default 'sudo mount -t vboxsf userhome /home/jason'

Launch all containers:

	docker-compose up

To get into a container:

	docker exec -it cc_php_api_1 bash

Making sure composer runs after mounting the volume:

 - The Dockerfile will run `composer install` on `/var/www/html`
 - Docker compose will replace the entire `/var/www/html` directory with a mounted volume, so you will need to run composer again.

	docker exec -it cc_php_api_1 bash
	composer install

To rebuild a specific container:

	docker-compose stop php_api
	docker-compose build --no-cache php_api
	docker-compose up -d --no-recreate php_api

Adding a file to Composer (from inside php_api):

	composer require league/oauth2-client
