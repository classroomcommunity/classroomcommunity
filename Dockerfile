FROM php:7.0-apache

# Install git and unzip for composer to use.
RUN apt-get update
RUN apt-get install -y git unzip

# Install PHP extensions.
RUN docker-php-ext-install mysqli pdo pdo_mysql

# Install PHP Composer
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php -r "if (hash_file('SHA384', 'composer-setup.php') === 'e115a8dc7871f15d853148a7fbac7da27d6c0030b848d9b3dc09e2a0388afed865e6a3d6b3c0fad45c48e2b5fc1196ae') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
RUN php composer-setup.php
RUN php -r "unlink('composer-setup.php');"
RUN mv composer.phar /usr/local/bin/composer

# Apache configuration
RUN a2enmod rewrite
RUN a2enmod headers

# Install the app
COPY www/ /var/www/html/

# Install composer dependencies.
RUN cd /var/www/html && composer install --no-interaction
