import sys.db.Mysql;
import php.Web;

class Main {
	public static function main() {
		var dbHost:String = untyped __php__("getenv('DB_NAME')");
		dbHost = dbHost.substr(dbHost.indexOf("/", 1)+1);
		var dbUser = untyped __php__("getenv('DB_ENV_MYSQL_USERNAME')");
		var dbPass = untyped __php__("getenv('DB_ENV_MYSQL_ROOT_PASSWORD')");
		var dbPort = untyped __php__("getenv('DB_PORT_3306_TCP_PORT')");
		var cnx = Mysql.connect({
			host: dbHost,
			port: dbPort,
			user: dbUser,
			pass: dbPass,
			database:"mysql"
		});
		trace('Connected: $cnx');
	}
}
